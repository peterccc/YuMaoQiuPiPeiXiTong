<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/9/13
  Time: 18:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>查看名次</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/zui/1.7.0/css/zui.min.css">
    <style>
        body {
            padding: 10px 0px;
            background-image: url("image/background.jpg");
        }
        th,td {
            font-size: 0.8em!important;
        }
    </style>
</head>
<body>
<div class="container">
    <table  class="table">
        <thead>
        <tr><th>名次</th><th>运动员</th><th>分数</th></tr>
        </thead>
        <tbody>
        <c:forEach items="${playerList}" var="player" varStatus="s">
            <tr>
                <td>${s.index + 1}</td>
                <td>${player.getName()}</td>
                <td>${player.getScore()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <br>
    <a class="btn" href="index.html?flag=show">返回</a>
    <br>
    <a class="btn" href="index.html">结束比赛</a>
    <div class="footer">
        <p>
            鸣谢：佛山中搜设计 @陈浩南 提供积分程序开发<br>
            （网站制作、广告设计、软件开发：13928660757）
        </p>
    </div>
</div>

</body>
</html>
