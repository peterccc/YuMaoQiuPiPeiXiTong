<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/9/13
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>开始比赛</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/zui/1.7.0/css/zui.min.css">
    <style>
        body {
            padding: 10px 0px;
            background-image: url("image/background.jpg");
        }
        th,td {
            font-size: 0.8em!important;
        }
    </style>
</head>
<body>
<div class="container">
    <form action="index.html" method="post">
        <input type="hidden" name="flag" value="save">
        <table class="table">
            <thead>
            <tr><th class="col-xs-1">No.</th><th>A队伍</th><th class="col-xs-2">A分数</th><th>B队伍</th><th class="col-xs-2">B分数</th></tr>
            </thead>
            <tbody>
            <c:forEach items="${matchList}" var ="li" varStatus="s">
                <tr>
                    <td>${s.index + 1}</td>
                    <td>${li.getFirstTeam()}</td>
                    <td><input class="form-control" type="text" name="team${li.getFirstTeam().getId()}" value="${li.getFirstTeam().getScore()}"></td>
                    <td>${li.getSecondTeam()}</td>
                    <td><input class="form-control" type="text" name="team${li.getSecondTeam().getId()}" value="${li.getSecondTeam().getScore()}"></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <br>
        <button class="btn" type="submit">保存</button>
    </form>

    <p>
        需要点击保存，保存数据后才查看排名，否则上面编辑的数据都不会被保存
    </p>
    <a class="btn" href="index.html?flag=ranking">查看排名</a>
    <br>
    <a class="btn" href="index.html">结束比赛</a>
    <div class="footer">
        <p>
            鸣谢：佛山中搜设计 @陈浩南 提供积分程序开发<br>
            （网站制作、广告设计、软件开发：13928660757）
        </p>
    </div>
</div>
</body>
</html>
