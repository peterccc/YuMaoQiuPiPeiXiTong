<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/9/15
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>创羽群</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/zui/1.7.0/css/zui.min.css">
    <style>
        html,body,div.container-cover,.cover {
            width: 100%;
            height: auto;
        }
        body{
            background-color: #000000;
        }
        div.container-cover {
            position: relative;
            top: calc(42% - 195px);
            padding: 0px;
        }
        div.container-cover form {
            position: absolute;
            right: 16.8%;
            bottom: 37%;
            width: 24.5%;
        }

        div.container-cover button {
            padding-left: 2px;
            padding-right: 2px;
        }
    </style>
</head>
<body>
<div class="container-fluid container-cover">
    <img class="cover" src="image/cover.jpg" alt="">
    <form action="" method="post">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="口令" name="password">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">GO</button>
            </span>
        </div>
    </form>
</div>
</body>
</html>
