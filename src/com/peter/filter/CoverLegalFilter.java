package com.peter.filter;

import com.peter.session.CoverLegalSession;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "CoverLegalFilter", urlPatterns = {"/*"})
public class CoverLegalFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse)resp;

        if(request.getRequestURI().equals("/cover.html") || request.getRequestURI().equals("/image/cover.jpg")) {
            chain.doFilter(req, resp);
            return;
        }

        CoverLegalSession coverLegalSession = new CoverLegalSession((HttpServletRequest) request, (HttpServletResponse) response);
        if(coverLegalSession.isLegal()) {
            chain.doFilter(req, resp);
        }else {
            response.sendRedirect("cover.html");
        }

    }

    public void init(FilterConfig config) throws ServletException {

    }

}
