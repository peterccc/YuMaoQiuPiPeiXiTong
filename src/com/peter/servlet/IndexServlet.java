package com.peter.servlet;

import com.peter.bean.Match;
import com.peter.bean.Player;
import com.peter.bean.Team;
import com.peter.service.AssignMatchService;
import com.sun.deploy.net.HttpResponse;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@WebServlet(name = "IndexServlet", urlPatterns = {"/index.html"})
public class IndexServlet extends HttpServlet {
    private void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AssignMatchService matchService = AssignMatchService.getInstance();
        matchService.resetPlayerScore();

        List<Match> matchList = matchService.getMatchList();
        Iterator<Match> iterator = matchList.iterator();
        while(iterator.hasNext()) {
            Match match = iterator.next();
            Team firstTeam = match.getFirstTeam();
            Team secondTeam = match.getSecondTeam();

            firstTeam.setScore(Integer.parseInt(request.getParameter("team" + firstTeam.getId())));
            secondTeam.setScore(Integer.parseInt(request.getParameter("team" + secondTeam.getId())));
        }

        response.sendRedirect("index.html?flag=show");
    }
    private void begin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] players = request.getParameterValues("players[]");
        AssignMatchService matchService = AssignMatchService.newInstance();

        for(String str: players){
            matchService.addPlayer(new Player(str));
        }

        matchService.match();

        response.sendRedirect("index.html?flag=show");

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flag = request.getParameter("flag");
        switch (flag) {
            case "save":
                save(request, response);
                break;
            case "begin":
                begin(request,response);
                break;
        }

    }

    private void ranking(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AssignMatchService assignMatchService = AssignMatchService.getInstance();
        List<Player> playerList = assignMatchService.getPlayerList();
        Collections.sort(playerList);
        request.setAttribute("playerList",playerList);
        request.getRequestDispatcher("/WEB-INF/ranking.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flag = request.getParameter("flag");
        flag = flag == null?"":flag;
        switch (flag) {
            default:
                request.getRequestDispatcher("/WEB-INF/number.jsp").forward(request,response);
                break;
            case "assign":
                String numberStr = request.getParameter("number");
                int number = Integer.parseInt(numberStr);
                request.setAttribute("number", number);
                request.getRequestDispatcher("/WEB-INF/assign.jsp").forward(request,response);
                break;
            case "ranking":
                ranking(request,response);
                break;
            case "show":
                request.setAttribute("matchList", AssignMatchService.getInstance().getMatchList());
                request.getRequestDispatcher("/WEB-INF/begin.jsp").forward(request,response);
                break;
        }


    }
}
