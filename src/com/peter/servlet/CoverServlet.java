package com.peter.servlet;

import com.peter.session.CoverLegalSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CoverServlet", urlPatterns = "/cover.html")
public class CoverServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String password = request.getParameter("password");
        CoverLegalSession coverSession = new CoverLegalSession(request, response);
        if(password.equals("创羽")) {
            // 正确
            coverSession.setLegal();
            response.sendRedirect("index.html");
        }else{
            // 错误
            coverSession.setLegal(false);
            response.sendRedirect("cover.html");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/cover.jsp").forward(request,response);
    }
}
