package com.peter.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CoverLegalSession {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HttpSession session;
    public CoverLegalSession(HttpServletRequest req, HttpServletResponse resp) {
        request = req;
        response = resp;
        session = request.getSession();
    }

    public void setLegal() {
        setLegal(true);
    }
    public void setLegal(boolean b) {
        session.setAttribute("cover_legal", b);
    }
    public boolean isLegal() {
        Object obj = session.getAttribute("cover_legal");
        return obj != null && (boolean) obj;
    }
}
