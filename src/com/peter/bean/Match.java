package com.peter.bean;

public class Match {
    private Team firstTeam;
    private Team secondTeam;


    public Match(Team firstTeam, Team secondTeam) {
        this.firstTeam = firstTeam;
        this.secondTeam = secondTeam;
    }

    public Team getFirstTeam() {
        return firstTeam;
    }

    public Team getSecondTeam() {
        return secondTeam;
    }

    public void setFirstTeam(Team firstTeam) {
        this.firstTeam = firstTeam;
    }

    public void setSecondTeam(Team secondTeam) {
        this.secondTeam = secondTeam;
    }

    @Override
    public String toString() {
        /*return "Match{\n" +
                "firstTeam=" + firstTeam +
                ", \nsecondTeam=" + secondTeam +
                "\n}";*/
        return "\n{" + firstTeam + ',' + secondTeam + '}';
    }


}
