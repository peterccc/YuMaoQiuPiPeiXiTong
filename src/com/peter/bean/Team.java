package com.peter.bean;

public class Team {
    private static int stId;
    private int id = ++stId;

    private Player firstPlayer;
    private Player secondPlayer;
    private int score;



    public Team(Player player) {
        firstPlayer = player;
    }
    public Team(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }

    public int getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        if (firstPlayer!=null)
            firstPlayer.setScore(firstPlayer.getScore() + score);
        if(secondPlayer!=null)
            secondPlayer.setScore(secondPlayer.getScore() + score);
        this.score = score;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }


    public Player getSecondPlayer() {
        return secondPlayer;
    }



    public boolean containerPlayer(Team team) {
        if(firstPlayer != null) {
            if(firstPlayer == team.firstPlayer) {
                // System.out.println("a:[" + this + ',' + team + ']');
                return true;
            }
            if(firstPlayer == team.secondPlayer) {
                // System.out.println("b:[" + this + ',' + team + ']');
                return true;
            }
        }

        if(secondPlayer != null) {
            if(secondPlayer == team.firstPlayer) {
                // System.out.println("c:[" + this + ',' + team + ']');
                return true;
            }
            if(secondPlayer == team.secondPlayer) {
                // System.out.println("d:[" + this + ',' + team + ']');
                return true;
            }
        }

        // System.out.println("{" + this + ',' + team + '}');
        // System.out.println("e========================:[" + this + ',' + team + ']');
        return false;

    }


    @Override
    public String toString() {
        /*return "Team{" +
                "\nfirstPlayer=" + firstPlayer +
                ", \nsecondPlayer=" + secondPlayer +
                //", \nscore=" + score +
                "\n}";*/
        if(firstPlayer == null){
            return "[" + secondPlayer + ']';
        }
        if(secondPlayer == null){
            return "[" + firstPlayer + ']';
        }
        return "[" + firstPlayer + ',' + secondPlayer + ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        // 如果有bug则删除这个
        if(id == team.id) return true;

//        if (firstPlayer != null ? !firstPlayer.equals(team.firstPlayer) : team.firstPlayer != null) return false;
//        return secondPlayer != null ? secondPlayer.equals(team.secondPlayer) : team.secondPlayer == null;
        if(firstPlayer == team.firstPlayer) {
            if(secondPlayer == team.secondPlayer) {
                return true;
            }
        }

        if(secondPlayer == team.firstPlayer) {
            if(firstPlayer == team.secondPlayer) {
                return true;
            }
        }

        return false;
    }


}

