package com.peter.bean;

public class Player implements Comparable<Player> {
    private static int stId = 0;
    private int id = ++stId;
    private String name;
    private int score;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Player() {

    }
    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        /*return "Player{" +
                //"\nid=" + id +
                ", \nname='" + name + '\'' +
                //", \nmatchCount=" + matchCount +
                "\n}";*/
        return name;
    }
    @Override
    public int compareTo(Player o) {
        return o.score - score;
    }

}
