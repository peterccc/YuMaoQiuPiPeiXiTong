package com.peter.service;

import com.peter.bean.Match;
import com.peter.bean.Player;
import com.peter.bean.Team;

import java.util.*;

public class AssignMatchService {
    private static AssignMatchService stAssignMatchService;

    private List<Match> matchList;
    private List<Team> teamList;
    private List<Player> playerList;

    public static AssignMatchService getInstance()  {
        return stAssignMatchService;
    }
    public static AssignMatchService newInstance() {
        stAssignMatchService = new AssignMatchService();
        return stAssignMatchService;
    }

    private AssignMatchService() {
        matchList = new ArrayList<>();
        teamList = new ArrayList<>();
        playerList = new ArrayList<>();
    }

    public boolean addPlayer(Player... players) {
        Collection<Player> c = Arrays.asList(players);
        playerList.addAll(c);
        return true;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public List<Match> getMatchList() {
        return matchList;
    }
    public List<Team> getTeamList() {return teamList;}
    public void resetPlayerScore() {
        for(Player player: playerList) {
            player.setScore(0);
        }
    }
    public void setTeamScore(int id, int score) {
        Team team = getTeam(id);
        team.setScore(score);
    }

    public Team getTeam(int id) {
        Iterator<Team> iterator = teamList.iterator();
        while(iterator.hasNext()) {
            Team team = iterator.next();
            if(id == team.getId()) {
                return team;
            }
        }
        return null;
    }

    public void match() {




        // 随机人员
        Collections.shuffle(playerList);

        // 分组
        assign(playerList, playerList, new AssignRule<Player, Player>() {
            @Override
            public boolean assign(Player obj1, Player obj2) {
                if(!obj1.equals(obj2)) {
                    Team team = new Team(obj1, obj2);
                    if(!teamList.contains(team)) {
                        teamList.add(team);
                    }
                }
                return false;
            }
        });
        // 随机组
        Collections.shuffle(teamList);
        List<Team> tmpTeamList = new ArrayList<>(teamList);



        // 分配哪个组vs 哪个组
        List<Team> errorTeamList = new ArrayList<>();
        while(tmpTeamList.size() > 1) {
            Team team1 = tmpTeamList.remove(0);
            int index = tmpTeamList.size();
            Team team2;
            do {
                --index;
                team2 = tmpTeamList.get(index);
            }while(team1.containerPlayer(team2) && index > 0);

            if(!team1.containerPlayer(team2)) {
                tmpTeamList.remove(index);
                matchList.add(new Match(team1, team2));
            }else {
                errorTeamList.add(team1);
            }
        }

        // 解决剩下的
        if(tmpTeamList.size() > 0) {
            Team team = tmpTeamList.remove(0);
            errorTeamList.add(team);
        }

        while(errorTeamList.size() > 1) {
            final Boolean[] assign = {false};
            assign(errorTeamList, errorTeamList, new AssignRule<Team, Team>() {
                @Override
                public boolean assign(Team obj1, Team obj2) {
                    if(!obj1.containerPlayer(obj2)) {
                        errorTeamList.remove(obj1);
                        errorTeamList.remove(obj2);
                        matchList.add(new Match(obj1, obj2));
                        assign[0] = true;
                        return true;
                    }
                    return false;
                }
            });
            if(!assign[0])  {
                assign(errorTeamList, matchList, new AssignRule<Team, Match>() {
                    @Override
                    public boolean assign(Team team, Match match) {
                        if(!match.getFirstTeam().containerPlayer(team) && !match.getSecondTeam().containerPlayer(team)){
                            Team fTeam1 = match.getFirstTeam();
                            Team fTeam2 = match.getSecondTeam();
                            match.setFirstTeam(team);
                            match.setSecondTeam(fTeam1);
                            errorTeamList.remove(team);
                            errorTeamList.add(fTeam2);
                            return true;
                        }else {
                            return false;
                        }
                    }
                });
            }




        }


        if(errorTeamList.size() > 0) {
            Team team = errorTeamList.remove(0);
            matchList.add(new Match(new Team(team.getFirstPlayer()),new Team(team.getSecondPlayer())));
        }

        // 找出所有组

    }

    private interface AssignRule<T1,T2> {
        boolean assign(T1 obj1, T2 obj2);
    }

    private <T1,T2> void assign(Collection<T1> col1,Collection<T2> col2, AssignRule<T1,T2> assignRule) {
        Iterator<T1> iterator1 = col1.iterator();

        outer:
        while(iterator1.hasNext()) {
            T1 obj = iterator1.next();
            Iterator<T2> iterator2 = col2.iterator();
            while(iterator2.hasNext()) {
                if(assignRule.assign(obj, iterator2.next())) {
                    break outer;
                }
            }
        }

    }
}
