<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/9/13
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>输入人数</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/zui/1.7.0/css/zui.min.css">
    <style>
        body {
            padding: 10px 0px;
            background-image: url("image/background.jpg");
        }
    </style>
</head>
<body>
<div class="container">
    <form method="get">
        <input type="hidden" name="flag" value="assign">
        <input type="text" name="number" class="form-control" placeholder="人数"><br>
        <button class="btn" type="submit">开始</button>
        <br>
        <a class="btn" href="index.html?flag=show">继续上一次比赛</a><br>
        <a class="btn" href="rule.jsp">比赛规则</a>
    </form>
    <div class="footer">
        <p>
            鸣谢：佛山中搜设计 @陈浩南 提供积分程序开发<br>
            （网站制作、广告设计、软件开发：13928660757）
        </p>
    </div>
</div>
</body>
</html>
