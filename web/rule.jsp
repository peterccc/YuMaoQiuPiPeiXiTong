<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/9/15
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>规则</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/zui/1.7.0/css/zui.min.css">
    <style>
        body {
            padding: 10px 0px;
            background-image: url("image/background-rule.jpg");
        }
    </style>
</head>
<body>
<div class="container">
<p >
    创羽群是2017年以私伙局快乐运动、汗水尽流为由创建。同期开展群积分赛，比赛分双打和单打，以双打为主，单打为增加比赛悬念和乐趣，最后以个人总积分决定排名。<br>
    <br>
    比赛规则：<br>
    1、单局21分制比赛 <br>
    2、比赛分A、B两组进行 <br>
    3、比赛前抽签决定球友在系统的排位<br>
    4、由系统计算出A、B组球友间的搭配<br>
    5、A组是背对裁判的左边，B组为右边<br>
    <br>
    比赛奖励：<br>
    一、每晚总积分前三名的分别依次奖励 红牛、宝矿力、健力宝 一支<br>
    二、4晚总积分前三名的分别依次奖励 yy手胶1盒（3条装）、运动袜子一对、yy手胶1条<br>
    三、10晚总积分前三名奖励羽毛球训练服一件<br>
    <br>
    创羽群 — 新朋友新赛法，老朋友新力量，为业余喜欢羽毛球运动的朋友提供一个友好和谐氛围的羽毛球活动群<br>
    <br>
    <a class="btn btn-default" href="javascript:history.go(-1)">返回</a>
    <div class="footer">
        <p>
            鸣谢：佛山中搜设计 @陈浩南 提供积分程序开发<br>
            （网站制作、广告设计、软件开发：13928660757）
        </p>
    </div>

</p>
</div>
</body>
</html>
